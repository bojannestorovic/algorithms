﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Algorithms
{
    public class GeometricAlgorithms
    {
        /// <summary>
        /// Calculates the angle between X axis and line L which consist of points a and b
        /// </summary>
        /// <param name="a">Point a of line L(a,b)</param>
        /// <param name="b">Point b of line L(a,b)</param>
        /// <returns></returns>
        public static double LineAngleWithXAxis(Point a, Point b)
        {
            var result = Math.Atan2((double)b.Y - a.Y, (double)b.X - a.X) * 180 / Math.PI;

            return result;
        }

        /// <summary>
        /// It finds extreme point (one with min value of Y, if there is tie, it looks most right one, with larger X value)
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static Point ExtremePoint(List<Point> points)
        {
            return points.Aggregate((a, b) => a.Y == b.Y && a.X > b.X ? a : (a.Y < b.Y ? a : b));
        }

        /// <summary>
        /// Finds simple polygon
        /// </summary>
        /// <param name="points">Points that will form simple polygon in random order</param>
        /// <returns></returns>
        public static List<Point> SimplePolygon(List<Point> points)
        {
            if (points == null || !points.Any())
                throw new Exception("Greska");

            //we find extreme point and save its index
            Point extremePoint = ExtremePoint(points);
            int s = points.IndexOf(extremePoint);

            //We take first point and put it at index of extreme point, and we remove extreme poin t from list
            var temp = points.First();
            points[s] = temp;
            points.RemoveAt(0);

            // We sort the list(excluding the extreme point) by the angle it forms with X axis, if there is a tie we take one with larger distance
            points.Sort((a, b) => {
                return CompareAngles(a, b, extremePoint);
            });

            // we insert back extreme point to begining of list
            points.Insert(0, extremePoint);

            return points;
        }


        /// <summary>
        /// Function that compares angles
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="extremePoint"></param>
        /// <returns></returns>
        public static int CompareAngles(Point a, Point b, Point extremePoint)
        {
            int k = TriPointOrientation(extremePoint, a, b);

            // We consider if orientation is collinear, different calculation based if it is in paralel with X or Y axis so correct distance would be calculated
            if (k == 0)
                if ((LineAngleWithXAxis(extremePoint, a) % 360 == 0) || (LineAngleWithXAxis(extremePoint, a) % 180 == 0))
                    return DistanceBetweenPoints(extremePoint, b) >= DistanceBetweenPoints(extremePoint, a) ? 1 : -1;
                else
                    return DistanceBetweenPoints(extremePoint, b) >= DistanceBetweenPoints(extremePoint, a) ? -1 : 1;

            return (k == 2) ? -1 : 1;
        }

        /// <summary>
        /// Distance between 2 points
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double DistanceBetweenPoints(Point a, Point b)
        {
            return (Math.Sqrt(Math.Pow((double)b.X - a.X, 2) + Math.Pow((double)b.Y - a.Y, 2)));
        }

        /// <summary>
        /// Finds point next to the top of stack without changing stack
        /// </summary>
        /// <param name="S">stack</param>
        /// <returns>next to top point</returns>
        public static Point NextToTop(Stack<Point> S)
        {
            Point p = S.Peek();
            S.Pop();
            Point res = S.Peek();
            S.Push(p);
            return res;
        }

        public static int TriPointOrientation(Point p, Point q, Point r)
        {
            // We use formula for calculating slope of lines p-q and q-r
            // formula is (y2-y1)/(x2-x1) < = >(less, equal or greater) ? (y3-y2)/(x3-x2) 
            // = (y2-y1)*(x3-x2) - (x2-x1)*(y3-y2) < = > 0 ?
            int val = (q.Y - p.Y) * (r.X - q.X) -
                (q.X - p.X) * (r.Y - q.Y);

            if (val == 0) return 0;  // colinear
            return (val > 0) ? 1 : 2; // clock(right turn) or counterclock wise (left turn)
        }

        /// <summary>
        /// It finds minimal convex hull for given points
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static List<Point> GrahamsAlgorithmForConvexHull(List<Point> points)
        {
            points = SimplePolygon(points);
            Stack<Point> stack = new Stack<Point>();
            stack.Push(points[0]);
            stack.Push(points[1]);
            stack.Push(points[2]);

            for (int k = 3; k < points.Count; k++)
            {
                // If the turn is right, we remove top of stack until the orientation changes
                // points for orientation are next to top from stack, top stack and current point from list
                while (TriPointOrientation(NextToTop(stack), stack.Peek(), points[k]) != 2)
                    stack.Pop();

                stack.Push(points[k]);
            }

            return stack.Reverse().ToList();
        }
    }
}
