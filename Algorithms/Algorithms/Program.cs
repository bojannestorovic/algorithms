﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Algorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Point> points = new List<Point>();
            points.Add(new Point(0, 0));
            points.Add(new Point(0, 2));
            points.Add(new Point(2, 0));
            points.Add(new Point(3, 0));
            points.Add(new Point(1, 2));
            points.Add(new Point(5, 2));
            points.Add(new Point(3, 1));
            points.Add(new Point(2, 1));
            points.Add(new Point(5, 3));
            points.Add(new Point(5, 0));

            Print("Original list:", points);
            Print("Simple polygon:", GeometricAlgorithms.SimplePolygon(points));
            Print("Grahams algorithm for convex hull:", GeometricAlgorithms.GrahamsAlgorithmForConvexHull(points));
        }

        public static void Print(string section, List<Point> points)
        {
            Console.WriteLine("");
            Console.WriteLine(section);
            foreach (var item in points)
            {
                Console.WriteLine($"({item.X},{item.Y})");
            }

            Console.WriteLine("===========================================================================");
        }
    }
}
